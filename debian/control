Source: md4c
Section: libs
Priority: optional
Maintainer: Debian Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>,
           Patrick Franz <deltaone@debian.org>,
           Andrea Pappacoda <andrea@pappacoda.it>,
Build-Depends: cmake, debhelper-compat (= 13), dh-sequence-pkgkde-symbolshelper,
Standards-Version: 4.7.0
Rules-Requires-Root: no
Homepage: https://github.com/mity/md4c
Vcs-Browser: https://salsa.debian.org/qt-kde-team/3rdparty/md4c
Vcs-Git: https://salsa.debian.org/qt-kde-team/3rdparty/md4c.git

Package: libmd4c-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libmd4c-html0-dev (= ${binary:Version}),
         libmd4c0 (= ${binary:Version}),
         ${misc:Depends},
Description: Markdown for C - development files
 MD4C is C Markdown parser with the following features:
 .
 Compliance: Generally MD4C aims to be compliant to the latest version of
 CommonMark specification. Right now fully compliant to CommonMark 0.28.
 .
 Extensions: MD4C supports some commonly requested and accepted extensions.
 .
 Compactness: MD4C is implemented in one source file and one header file.
 .
 Embedding: MD4C is easy to reuse in other projects, its API is very
 straightforward: There is actually just one function, md_parse().
 .
 Push model: MD4C parses the complete document and calls callback functions
 provided by the application for each start/end of block, start/end of a span,
 and with any textual contents.
 .
 Portability: MD4C builds and works on Windows and Linux, and it should
 be fairly simple to make it run also on most other systems.
 .
 Encoding: MD4C can be compiled to recognize ASCII-only control characters,
 UTF-8 and, on Windows, also UTF-16, i.e. what is on Windows commonly
 called just "Unicode". See more details below.
 .
 Permissive license: MD4C is available under the MIT license.
 .
 This package ships the library's development files.

Package: libmd4c0
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends},
Description: Markdown for C
 MD4C is C Markdown parser with the following features:
 .
 Compliance: Generally MD4C aims to be compliant to the latest version of
 CommonMark specification. Right now fully compliant to CommonMark 0.28.
 .
 Extensions: MD4C supports some commonly requested and accepted extensions.
 .
 Compactness: MD4C is implemented in one source file and one header file.
 .
 Embedding: MD4C is easy to reuse in other projects, its API is very
 straightforward: There is actually just one function, md_parse().
 .
 Push model: MD4C parses the complete document and calls callback functions
 provided by the application for each start/end of block, start/end of a span,
 and with any textual contents.
 .
 Portability: MD4C builds and works on Windows and Linux, and it should
 be fairly simple to make it run also on most other systems.
 .
 Encoding: MD4C can be compiled to recognize ASCII-only control characters,
 UTF-8 and, on Windows, also UTF-16, i.e. what is on Windows commonly
 called just "Unicode". See more details below.
 .
 Permissive license: MD4C is available under the MIT license.

Package: libmd4c-html0-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libmd4c-html0 (= ${binary:Version}), ${misc:Depends},
Description: Markdown for C HTML-renderer - development files
 MD4C is C Markdown parser with the following features:
 .
 Compliance: Generally MD4C aims to be compliant to the latest version of
 CommonMark specification. Right now fully compliant to CommonMark 0.28.
 .
 Extensions: MD4C supports some commonly requested and accepted extensions.
 .
 Compactness: MD4C is implemented in one source file and one header file.
 .
 Embedding: MD4C is easy to reuse in other projects, its API is very
 straightforward: There is actually just one function, md_parse().
 .
 Push model: MD4C parses the complete document and calls callback functions
 provided by the application for each start/end of block, start/end of a span,
 and with any textual contents.
 .
 Portability: MD4C builds and works on Windows and Linux, and it should
 be fairly simple to make it run also on most other systems.
 .
 Encoding: MD4C can be compiled to recognize ASCII-only control characters,
 UTF-8 and, on Windows, also UTF-16, i.e. what is on Windows commonly
 called just "Unicode". See more details below.
 .
 Permissive license: MD4C is available under the MIT license.
 .
 This package ships the standalone Markdown-to-HTML converter's development
 files.

Package: libmd4c-html0
Architecture: any
Multi-Arch: same
Depends: libmd4c0 (= ${binary:Version}), ${misc:Depends}, ${shlibs:Depends},
Description: Markdown for C HTML-renderer
 MD4C is C Markdown parser with the following features:
 .
 Compliance: Generally MD4C aims to be compliant to the latest version of
 CommonMark specification. Right now fully compliant to CommonMark 0.28.
 .
 Extensions: MD4C supports some commonly requested and accepted extensions.
 .
 Compactness: MD4C is implemented in one source file and one header file.
 .
 Embedding: MD4C is easy to reuse in other projects, its API is very
 straightforward: There is actually just one function, md_parse().
 .
 Push model: MD4C parses the complete document and calls callback functions
 provided by the application for each start/end of block, start/end of a span,
 and with any textual contents.
 .
 Portability: MD4C builds and works on Windows and Linux, and it should
 be fairly simple to make it run also on most other systems.
 .
 Encoding: MD4C can be compiled to recognize ASCII-only control characters,
 UTF-8 and, on Windows, also UTF-16, i.e. what is on Windows commonly
 called just "Unicode". See more details below.
 .
 Permissive license: MD4C is available under the MIT license.
 .
 This package ships the standalone Markdown-to-HTML converter.

Package: md2html
Section: text
Architecture: any
Multi-Arch: no
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: Markdown to HTML converter
 md2html is a command line utility based on the MD4C library which converts
 Markdown files to HTML.
 .
 It supports the CommonMark and GitHub Flavored dialects of Markdown, and has
 multiple options enabling different extensions like URL autolinking,
 strikethrough, tables, and more.
 .
 Being based on the MD4C library, it benefits from its performance and
 portability. For more information about the MD4C library, see the libmd4c-dev
 package.
